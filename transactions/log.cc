#include "log.h"

Logger::Logger(int level) : level_(level), used_(false) {}

Logger::~Logger() noexcept {
  try {
    if (is_enabled()) {
      get_underlying() << std::endl;
    }
  } catch (...) {
    // ¯\_(ツ)_/¯
  }
}

bool Logger::is_enabled() const { return LOG_VERBOSITY <= level_; }

std::ostream &Logger::get_underlying() {
  if (!used_) {
    used_ = true;
    std::cerr << format_level() << "\t";
  }
  return std::cerr;
}

std::string Logger::format_level() const {
  switch (level_) {
  case DEBUG_LOG_VERBOSITY:
    return "DEBUG";
  case INFO_LOG_VERBOSITY:
    return "INFO";
  case ERROR_LOG_VERBOSITY:
    return "ERROR";
  default:
    return "UNKNOWN";
  }
}
