#include <cassert>
#include <exception>
#include <tuple>

#include "env.h"
#include "log.h"

////////////////////////////////////////////////////////////////////////////////

bool Env::DeliveredLast::operator()(const SentMessage &lhs,
                                    const SentMessage &rhs) const {
  return std::make_tuple(lhs.delivery_timestamp, lhs.send_timestamp, lhs.source,
                         lhs.destination) >
         std::make_tuple(rhs.delivery_timestamp, rhs.send_timestamp, rhs.source,
                         rhs.destination);
}

////////////////////////////////////////////////////////////////////////////////

Env::Env(Timestamp mean_delivery_delay, double drop_probability, int generator_seed)
    : drop_probability_(drop_probability), generator_(generator_seed),
      delivery_delay_distribution_(1.0 / mean_delivery_delay),
      drop_distribution_(0.0, 1.0), timestamp_(0) {
  assert(0.0 <= drop_probability && drop_probability <= 1.0);
}

void Env::register_actor(IActor *actor) {
  auto id = actor->get_id();
  if (!actor_messages_.emplace(id, std::vector<Message>()).second) {
    std::terminate();
  }
  actors_.push_back(actor);
}

void Env::send(Message msg) {
  ++sent_message_count_;
  LOG_DEBUG << "[ts " << timestamp_ << "] Send message of type \""
            << format_message_type(msg.type) << "\" from " << msg.source
            << " to " << msg.destination;
  messages_queue_.push(SentMessage{std::move(msg), timestamp_,
                                   timestamp_ + generate_delivery_delay()});
}

void Env::run(int ticks) {
  for (Timestamp tick = 0; tick < ticks; ++tick) {
    for (auto &[id, messages] : actor_messages_) {
      messages.clear();
    }

    timestamp_ = clock_.current();
    while (!messages_queue_.empty() &&
           messages_queue_.top().delivery_timestamp <= timestamp_) {
      auto msg = messages_queue_.top();
      messages_queue_.pop();

      auto it = actor_messages_.find(msg.destination);
      if (it == actor_messages_.end()) {
        std::terminate();
      }

      bool dropped = should_drop();

      LOG_DEBUG << "[ts " << timestamp_ << "] "
                << (dropped ? "Drop" : "Deliver") << " message of type \""
                << format_message_type(msg.type) << "\" from " << msg.source
                << " to " << msg.destination;

      if (!dropped) {
        it->second.push_back(std::move(msg));
      }
    }

    for (const auto &actor : actors_) {
      actor->on_tick(clock_, std::move(actor_messages_[actor->get_id()]));
    }

    if (clock_.current() == timestamp_) {
      clock_.next();
    }
  }
}

int Env::get_sent_message_count() const { return sent_message_count_; }

Timestamp Env::generate_delivery_delay() {
  return std::max(Timestamp(1), static_cast<Timestamp>(
                                    delivery_delay_distribution_(generator_)));
}

bool Env::should_drop() {
  return drop_distribution_(generator_) < drop_probability_;
}

////////////////////////////////////////////////////////////////////////////////

EnvProxy::EnvProxy(Env *env, ActorId source) : env_(env), source_(source) {}

void EnvProxy::send(Message msg) {
  msg.source = source_;
  env_->send(std::move(msg));
}

////////////////////////////////////////////////////////////////////////////////
