#include <unordered_map>

#include "client.h"
#include "log.h"

Client::Client(ActorId id, std::vector<ClientTransactionSpec> tx_specs,
               const Discovery *discovery, EnvProxy env)
    : id_(id), env_(env), retrier_(123 + id) {
  transactions_.reserve(tx_specs.size());
  for (auto &spec : tx_specs) {
    transactions_.push_back(std::make_unique<ClientTransaction>(
        id_, std::move(spec), discovery, &retrier_));
  }
}

const ClientTransaction &Client::get_tx(int index) const {
  return *transactions_[index];
}

ActorId Client::get_id() const { return id_; }

void Client::on_tick(Clock &clock, std::vector<Message> messages) {
  std::unordered_map<TransactionId, std::vector<Message>> messages_per_tx;
  for (auto &msg : messages) {
    auto txid = get_message_txid(msg);
    if (txid == UNDEFINED_TRANSACTION_ID) {
      report_unexpected_message(msg);
      continue;
    }
    messages_per_tx[txid].push_back(std::move(msg));
  }
  std::vector<Message> outgoing_messages;
  for (auto &tx : transactions_) {
    auto txid = tx->get_id();
    std::vector<Message> tx_messages;
    if (txid != UNDEFINED_TRANSACTION_ID) {
      auto it = messages_per_tx.find(txid);
      if (it != messages_per_tx.end()) {
        tx_messages = std::move(it->second);
      }
    }
    tx->tick(clock.next(), tx_messages, &outgoing_messages);
  }
  for (const auto &[txid, tx_messages] : messages_per_tx) {
    for (const auto &msg : tx_messages) {
      report_unexpected_message(msg);
    }
  }
  for (auto &msg : outgoing_messages) {
    env_.send(std::move(msg));
  }
}

TransactionId Client::get_message_txid(const Message &msg) const {
  switch (msg.type) {
  case MSG_START_ACK: {
    return msg.get<MessageStartAckPayload>().txid;
  }
  case MSG_GET_REPLY: {
    return msg.get<MessageGetReplyPayload>().txid;
  }
  case MSG_PUT_REPLY: {
    return msg.get<MessagePutReplyPayload>().txid;
  }
  case MSG_COMMIT_ACK: {
    return msg.get<MessageCommitAckPayload>().txid;
  }
  case MSG_ROLLBACK_ACK: {
    return msg.get<MessageRollbackAckPayload>().txid;
  }
  case MSG_ROLLED_BACK_BY_SERVER: {
    return msg.get<MessageRolledBackByServerPayload>().txid;
  }
  default:
    return UNDEFINED_TRANSACTION_ID;
  }
}

void Client::report_unexpected_message(const Message &msg) const {
  LOG_ERROR << "Client " << msg.destination
            << " got un unexpected message of type \""
            << format_message_type(msg.type) << "\" from " << msg.source;
}
