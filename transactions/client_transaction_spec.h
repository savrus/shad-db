
#pragma once

#include "types.h"

// ClientTransactionSpec describes what should be done in a given
// transaction. It lists all Get and Put requests that the client
// will perform in the transaction.

// It also specifies whether the transaction is committed or rolled
// back in the end.

struct ClientTransactionGet {
  Timestamp earliest_timestamp;
  Key key;
};

struct ClientTransactionPut {
  Timestamp earliest_timestamp;
  Key key;
  Value value;
};

struct ClientTransactionSpec {
  // Earliest timestamp to start the transaction.
  Timestamp earliest_start_timestamp;

  // Earliest timestamp to commit the transaction.
  Timestamp earliest_commit_timestamp;

  std::vector<ClientTransactionGet> gets;
  std::vector<ClientTransactionPut> puts;

  enum { UNDEFINED, COMMIT, ROLLBACK } action;
};
