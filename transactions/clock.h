#pragma once

#include "types.h"

// Clock is a source of timestamp: it provides a monotonically increasing stream
// of timestamps.
class Clock {
public:
  Timestamp current() const;
  Timestamp next();

private:
  Timestamp next_{0};
};
