#include "ast.h"

#include <cassert>

namespace shdb {

Ast::Ast(Type type) : type(type)
{}

String::String(Type type, std::string value) : Ast(type), value(value)
{}

Number::Number(int value) : Ast(Type::number), value(value)
{}

Binary::Binary(Opcode op, std::shared_ptr<Ast> lhs, std::shared_ptr<Ast> rhs)
    : Ast(Type::binary), op(op), lhs(lhs), rhs(rhs)
{}

Unary::Unary(Opcode op, std::shared_ptr<Ast> operand) : Ast(Type::unary), op(op), operand(operand)
{}

List::List() : Ast(Type::list)
{}

List::List(std::shared_ptr<Ast> element) : Ast(Type::list), list{element}
{}

void List::append(std::shared_ptr<Ast> element)
{
    list.push_back(element);
}

Function::Function(std::string name, std::shared_ptr<List> args)
    : Ast(Type::function), name(std::move(name)), args(std::move(args))
{}

Select::Select(
    std::shared_ptr<List> list,
    std::shared_ptr<Ast> where)
    : Ast(Type::select)
    , list(list)
    , where(std::move(where))
{}


std::shared_ptr<Ast> new_name(std::string value)
{
    return std::make_shared<String>(Type::name, std::move(value));
}

std::shared_ptr<Ast> new_string(std::string value)
{
    return std::make_shared<String>(Type::string, std::move(value));
}

std::shared_ptr<Ast> new_number(int value)
{
    return std::make_shared<Number>(value);
}

std::shared_ptr<Ast> new_binary(Opcode op, std::shared_ptr<Ast> lhs, std::shared_ptr<Ast> rhs)
{
    return std::make_shared<Binary>(op, lhs, rhs);
}

std::shared_ptr<Ast> new_unary(Opcode op, std::shared_ptr<Ast> operand)
{
    return std::make_shared<Unary>(op, operand);
}

std::shared_ptr<List> new_list()
{
    return std::make_shared<List>();
}

std::shared_ptr<List> new_list(std::shared_ptr<Ast> operand)
{
    return std::make_shared<List>(operand);
}

std::shared_ptr<Ast> new_function(std::string name, std::shared_ptr<List> args)
{
    return std::make_shared<Function>(std::move(name), std::move(args));
}

std::shared_ptr<Ast> new_select(
    std::shared_ptr<List> list,
    std::shared_ptr<Ast> where)
{
    return std::make_shared<Select>(
        list, std::move(where));
}

std::string to_string(std::shared_ptr<Ast> ast)
{
    if (!ast) {
        return {};
    }

    switch (ast->type) {
    case Type::name:
        return std::static_pointer_cast<String>(ast)->value;
    case Type::string:
        return std::string("\"") + std::static_pointer_cast<String>(ast)->value + "\"";
    case Type::number:
        return std::to_string(std::static_pointer_cast<Number>(ast)->value);
    case Type::binary: {
        auto op = std::static_pointer_cast<Binary>(ast);
        auto get_op = [&]() -> std::string {
            switch (op->op) {
            case Opcode::plus:
                return "+";
            case Opcode::minus:
                return "-";
            case Opcode::mul:
                return "*";
            case Opcode::div:
                return "/";
            case Opcode::land:
                return "AND";
            case Opcode::lor:
                return "OR";
            case Opcode::eq:
                return "=";
            case Opcode::ne:
                return "<>";
            case Opcode::lt:
                return "<";
            case Opcode::le:
                return "<=";
            case Opcode::gt:
                return ">";
            case Opcode::ge:
                return ">=";
            default:
                assert(0);
            }
        };
        return std::string("(") + to_string(op->lhs) + ") " + get_op() + " (" + to_string(op->rhs) + ")";
    }
    case Type::unary: {
        auto op = std::static_pointer_cast<Unary>(ast);
        auto get_op = [&]() -> std::string {
            switch (op->op) {
            case Opcode::lnot:
                return "!";
            case Opcode::uminus:
                return "-";
            default:
                assert(0);
            }
        };
        return get_op() + " (" + to_string(op->operand) + ")";
    }
    case Type::list: {
        auto op = std::static_pointer_cast<List>(ast);
        std::string result = to_string(op->list[0]);
        for (size_t index = 1; index < op->list.size(); ++index) {
            result += ", " + to_string(op->list[index]);
        }
        return result;
    }
    case Type::function: {
        auto fun = std::static_pointer_cast<Function>(ast);
        return fun->name + "(" + (fun->args ? to_string(fun->args) : "") + ")";
    }
    case Type::select: {
        auto op = std::static_pointer_cast<Select>(ast);
        auto result = std::string("SELECT ") + (op->list ? to_string(op->list) : "*");
        if (op->where) {
            result += " WHERE " + to_string(op->where);
        }
        return result;
    }
    default: {
        assert(0);
    }
    }
}

}    // namespace shdb
