#include "rowset.h"

#include "row.h"

#include <iostream>
#include <vector>

namespace shdb {

Rowset::Rowset()
{}

Rowset::Rowset(std::shared_ptr<shdb::Schema> schema) : schema(std::move(schema))
{}

Rowset::Rowset(Rowset &&other)
{
    schema = std::move(other.schema);
    rows = std::move(other.rows);
}

Row *Rowset::allocate()
{
    auto *row = new shdb::Row(schema->size());
    rows.push_back(row);
    return row;
}

Rowset::~Rowset()
{
    for (auto *row : rows) {
        delete row;
    }
}

}    // namespace shdb
