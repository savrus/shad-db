#include "shdb/db.h"
#include "shdb/sql.h"

#include <stdio.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/wait.h>

#include <cassert>
#include <chrono>
#include <iostream>
#include <sstream>

auto fixed_schema = std::make_shared<shdb::Schema>(shdb::Schema{{"id", shdb::Type::uint64},
                                                                {"name", shdb::Type::varchar, 1024},
                                                                {"age", shdb::Type::uint64},
                                                                {"graduated", shdb::Type::boolean}});

std::shared_ptr<shdb::Database> create_database(int frame_count)
{
    auto db = shdb::connect("./mydb", frame_count);
    if (db->check_table_exists("test_table")) {
        db->drop_table("test_table");
    }
    db->create_table("test_table", fixed_schema);
    return db;
}

void test_ddl()
{
    auto db = create_database(1);
    assert(db->check_table_exists("test_table"));

    auto sql = shdb::Sql(db);
    sql.execute("DROP TABLE test_table");
    assert(!db->check_table_exists("test_table"));

    sql.execute("CREATE TABLE test_table (id uint64, name string, nick varchar(44), flag boolean)");
    assert(db->check_table_exists("test_table"));
    auto schema = db->find_table_schema("test_table");
    assert(schema);

    auto &columns = *schema;
    assert(columns.size() == 4);
    assert(columns[0].name == "id" && columns[0].type == shdb::Type::uint64 && columns[0].length == 0);
    assert(columns[1].name == "name" && columns[1].type == shdb::Type::string && columns[1].length == 0);
    assert(columns[2].name == "nick" && columns[2].type == shdb::Type::varchar && columns[2].length == 44);
    assert(columns[3].name == "flag" && columns[3].type == shdb::Type::boolean && columns[3].length == 0);

    sql.execute("DROP TABLE test_table");
    assert(!db->check_table_exists("test_table"));

    std::cout << "Test ddl passed" << std::endl;
}

void cmd()
{
    auto db = shdb::connect("./mydb", 1);
    auto sql = shdb::Sql(db);

    while (!std::cin.eof()) {
        std::cout << "shdb> " << std::flush;
        std::string line;
        std::getline(std::cin, line);
        if (!line.empty()) {
            try {
                auto rowset = sql.execute(line);
                for (auto *row : rowset.rows) {
                    std::cout << to_string(*row) << std::endl;
                }
            } catch (char const *ex) {
                std::cout << "Error: " << ex << std::endl;
            }
        }
    }
}

int main(int argc, char *argv[])
{
    test_ddl();
}
