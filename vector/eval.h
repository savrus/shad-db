#pragma once

#include "ast.h"
#include "schema.h"
#include "table.h"

namespace shdb {

class Eval
{
    // Your code goes here

public:
    Eval(std::shared_ptr<Schema> schema, std::shared_ptr<Table> table, std::shared_ptr<Select> select);
    std::shared_ptr<Table> run();
};

}    // namespace shdb
