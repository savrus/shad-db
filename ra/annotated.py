#!/usr/bin/python3
# -*- coding: utf-8 -*-

import json
from collections import OrderedDict

def dump_text(filename, annotated_objects):
    with open(filename, "w", encoding="utf-8") as f:
        skip = ""
        for key, value in annotated_objects.items():
            f.write("{0}# {1}\n{2}\n".format(skip, key, value))
            skip = "\n"

def load_text(filename):
    with open(filename, "r", encoding="utf-8") as f:
        lines = f.readlines()
    annotated_objects = OrderedDict()
    key = None
    for line in lines:
        if line.startswith("# "):
            key = line.strip()[2:]
            if len(key) == 0:
                raise Exception("Empyt name is not allowed")
            if key in annotated_objects:
                raise Exception("Duplicated name \"{0}\"".format(key))
            annotated_objects[key] = ""
        elif key:
            annotated_objects[key] += line
    for key in annotated_objects.keys():
        annotated_objects[key] = annotated_objects[key].strip()
    return annotated_objects

def dump_json(filename, annotated_objects):
    annotated_jsons = OrderedDict((key, json.dumps(obj)) for key, obj in annotated_objects.items())
    dump_text(filename, annotated_jsons)

def load_json(filename):
    annotated_jsons = load_text(filename)
    return OrderedDict((key, json.loads(obj)) for key, obj in annotated_jsons.items())
