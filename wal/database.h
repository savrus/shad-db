#pragma once

#include <string>
#include <unordered_map>

class database {
public:
  explicit database(const char* filename);

  void set(std::string_view key, std::string_view value);
  void erase(std::string_view key);

  const std::unordered_map<std::string, std::string>& values() const {
    return values_;
  }

private:
  std::unordered_map<std::string, std::string> values_;
};
