
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>

int open_tcp(int port) {
  // Open a TCP socket, bind to the given port, and return the socket number
  return 0;
}

int main(int argc, char* argv[]) {
  if (argc < 2) {
    fprintf(stderr, "Usage: main <port number>\n");
    exit(1);
  }

  int port = atoi(argv[1]);

  open_tcp(port);

  // Accept incoming clients, and serve requests.

  return 0;
}
