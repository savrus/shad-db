#!/bin/bash

set -e
set -x

source usernames.sh

git clone git@gitlab.com:savrus/shad-db.git

for username in ${usernames[@]}
do
  url="git@gitlab.manytask.org:db/students-spring-2023/${username}.git"
  git clone ${url}
  cd ${username}
  git checkout submits/threads
  cd ..
  echo "Done checking out ${username}"
done
