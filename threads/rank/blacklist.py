#!/usr/bin/python

import sys

blacklist = (
)

if (sys.argv[1], int(sys.argv[2])) in blacklist or (sys.argv[1], ) in blacklist:
  sys.exit(0)

sys.exit(1)
