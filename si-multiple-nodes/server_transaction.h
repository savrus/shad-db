#pragma once

// ServerTransaction implements server-side state machine for a transaction.
//
// Transactions are allowed to span multiple servers. In that case,
// each server will maintain its own ServerTransaction object for the
// transaction.

#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "message.h"
#include "retrier.h"

class Storage;

enum class ServerTransactionState {
  // Blank transaction. Transaction is created in the NOT_STARTED state,
  // and immediately switches to OPEN state upon processing the Start message.
  NOT_STARTED,

  // Start is received, read_timestamp is assigned.
  // In OPEN state, transaction may serve Get and Put requests.
  OPEN,

  // In a PREPARED state, the transaction received a Prepare message from the 
  // coordinator. It checked all writes and reads, and found no conflicts.
  // It assigned commit_timestamp to the transaction, and replied with
  // PrepareAck message to the coordinator.
  //
  // PREPARED state only applies to non-coordinator participants in a distributed
  // transaction.
  PREPARED,

  // In a ROLLBACK state, the transaction received a Rollback message.
  // It will never apply its writes after moving into this state.
  ROLLBACK,

  // Upon receiving a Commit or Prepare, this transaction detected a conflict with
  // another transaction.
  //
  // Similarly, to the ROLLBACK state, a ROLLED_BACK_BY_SERVER transaction will
  // never commit. However, ROLLBACK states are always initiated by the client,
  // while ROLLED_BACK_BY_SERVER designates states when a transaction is aborted
  // due to conflict, detected by the server.
  ROLLED_BACK_BY_SERVER,

  // In this state, the transaction received a Commit message from the client.
  // It assigned commit_timestamp, and sent Prepare message to all other participants. 
  //
  // Now it waits each participant to reply with either PrepareAck message,
  // or with Conflict message.
  //
  // Upon seeing a PrepareAck message from all participants, the transaction should
  // switch to the COMMITTED state. If any participant replies with the Conflict
  // message, the transaction switches to the ROLLED_BACK_BY_SERVER state.
  //
  // This state only applies to coordinator nodes.
  COORDINATOR_COMMITTING,

  // Transaction is committed.
  COMMITTED,
};

std::string format_server_transaction_state(ServerTransactionState state);

class ServerTransaction {
public:
  ServerTransaction(ActorId self, Storage *storage, IRetrier *rt);

  void tick(Timestamp ts, const std::vector<Message> &messages,
            std::vector<Message> *msg_out);

  // id of this transaction.
  // Different ServerTransaction objects have the same txid, when they
  // run on different servers, but belong to the same transaction.
  TransactionId txid() const { return txid_; }

  ActorId id() const { return self_; }
  ServerTransactionState state() const { return state_; }

private:
  void process_message_not_started(Timestamp ts, const Message &msg);
  void process_message_open(Timestamp ts, const Message &msg);
  void process_message_rollback(Timestamp ts, const Message &msg);
  void process_message_coordinator_committing(Timestamp ts, const Message &msg);
  void process_message_commited(Timestamp ts, const Message &msg);
  void process_message_rolled_back_by_server(Timestamp ts, const Message &msg);
  void process_message_prepared(Timestamp ts, const Message &msg);

  void report_unexpected_msg(const Message &msg);

  const ActorId self_;
  Storage *storage_;
  IRetrier *rt_;

  ServerTransactionState state_{ServerTransactionState::NOT_STARTED};
  TransactionId txid_{-1};
  ActorId client_{-1};

  Timestamp read_timestamp_;
  Timestamp commit_timestamp_;

  // TODO: add state related to ServerTransaction functioning.
};
