Выведите название, площадь, цену здания, а также название региона здания,
для всех зданий, площадь которых больше 30, а цена меньше 50.
1.---
SELECT 1
---

Выведите названия всех зданий, в материалах которых присутствуют золото ("gold")
или мрамор ("marble").
2.---
SELECT 1
---

Выведите имена и фамилии покупателей, которые не хотят приобрести недвижимость
в Геленджике ("Gelendzhik").
3.---
SELECT 1
---

Для каждого покупателя выведите его имя, фамилию и название материала, который не встречается
ни в одном его заказе. Если таких материалов несколько, выведите строчку для каждого материала.
Если таких материалов для данного покупателя нет, ничего не выводите.
4.---
SELECT 1
---

Выведите имена городов, в которых средняя площадь объекта недвижимости выше, чем средняя площадь 
по всем объектам невижимости (во всех городах).
5.---
SELECT 1
---

Для каждого владельца недвижимости выведите его имя, фамилию и цену самого дорогого объекта
среди всех его объектов. Если у владельца нет объектов, выведите NULL.
6.---
SELECT 1
---

Выведите имена и фамилии всех покупателей, которые готовы купить здание 
под именем "Object 101".
7.---
SELECT 1
---

Для каждого города выведите его название и количество объектов в этом городе,
на которые есть хотя бы один покупатель.
8.---
SELECT 1
---