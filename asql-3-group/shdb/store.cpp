#include "store.h"

namespace shdb {

Store::Store(const std::filesystem::path &path, FrameIndex frame_count, std::shared_ptr<Statistics> statistics)
    : buffer_pool(std::make_shared<BufferPool>(std::move(statistics), frame_count)), path(path)
{}

void Store::create_table(const std::filesystem::path &name)
{
    auto file = std::make_unique<File>(path / name, true);
}

std::shared_ptr<Table> Store::open_table(const std::filesystem::path &name, PageProvider provider)
{
    auto file = std::make_unique<File>(path / name, false);
    return shdb::create_table(buffer_pool, std::move(file), std::move(provider));
}

bool Store::check_table_exists(const std::filesystem::path &name)
{
    return std::filesystem::exists(path / name);
}

void Store::remove_table(const std::filesystem::path &name)
{
    std::filesystem::remove(path / name);
}

}    // namespace shdb
