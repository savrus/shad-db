#pragma once

#include "schema.h"
#include "store.h"
#include "table.h"

namespace shdb {

class Catalog
{
    // Your code goes here.

public:
    explicit Catalog(std::shared_ptr<Store> store);

    void save_table_schema(const std::filesystem::path &name, std::shared_ptr<Schema> schema);
    std::shared_ptr<Schema> find_table_schema(const std::filesystem::path &name);
    void forget_table_schema(const std::filesystem::path &name);
};

}    // namespace shdb
