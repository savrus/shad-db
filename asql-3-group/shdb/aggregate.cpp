#include "aggregate.h"

#include "flow.h"
#include "schema.h"

namespace shdb {

Aggregate::Aggregate(Jit &jit, Type type) : jit(jit), type(type)
{}


// Your code goes here


std::unique_ptr<Aggregate> create_aggregate(Jit &jit, const std::string &name, Type type)
{
    if (name == "sum") {
        return std::make_unique<Sum>(jit, type);
    } else if (name == "min") {
        return std::make_unique<Min>(jit, type);
    } else if (name == "max") {
        return std::make_unique<Max>(jit, type);
    } else if (name == "avg") {
        return std::make_unique<Avg>(jit, type);
    } else {
        throw "Unknown function";
    }
}

}    // namespace shdb
