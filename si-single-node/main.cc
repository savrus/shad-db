
#include "scenario.h"

namespace {

void run_scenario_simple_one_node() {
  run_scenario_simple(
    /* name */ "simple one node",
    /* key_intervals */ {{KEY_LOWER_BOUND, KEY_UPPER_BOUND}},
    /* drop_probability */ 0.0);
}

void run_scenario_multiple_transactions_one_node() {
  run_scenario_multiple_transactions(
    "multiple transactions one node",
    /* key_intervals */ {{KEY_LOWER_BOUND, KEY_UPPER_BOUND}},
    /* drop_probability */ 0.0);
}

void run_scenario_race_one_node() {
  run_scenario_race(
    "race one node",
    /* key_intervals */ {{KEY_LOWER_BOUND, KEY_UPPER_BOUND}},
    /* drop_probability */ 0.0);
}

}  // namespace

int main(int argc, char* argv[]) {
  run_scenario_simple_one_node();
  run_scenario_multiple_transactions_one_node();
  
  // As of 2022, solutions may not necessarily pass this scenario.
  // run_scenario_race_one_node();
  
  run_scenario_randomized(
      /* n_servers */ 1,
      /* drop_probability */ 0.0,
      /* n_clients */ 8,
      /* transactions per client */ 10,
      /* gets per transaction */ 10,
      /* puts per transaction */ 10);
  return 0;
}
