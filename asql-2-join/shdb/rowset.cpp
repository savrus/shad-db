#include "rowset.h"

#include "row.h"

#include <iostream>
#include <vector>

namespace shdb {

Rowset::Rowset()
{}

Rowset::Rowset(std::shared_ptr<Schema> schema) : schema(std::move(schema))
{}

Rowset::Rowset(Rowset &&other)
{
    schema = std::move(other.schema);
    rows = std::move(other.rows);
}

Row *Rowset::allocate()
{
    auto *row = new Row(schema->size());
    rows.push_back(row);
    return row;
}

void Rowset::sort_rows(int (*comparer)(const Row *lhs, const Row *rhs))
{
    std::sort(rows.begin(), rows.end(), [&](auto *lhs, auto *rhs) { return comparer(lhs, rhs) < 0; });
}

Rowset::~Rowset()
{
    for (auto *row : rows) {
        delete row;
    }
}


Hashset::Hashset()
{}

Hashset::Hashset(std::shared_ptr<Schema> schema) : schema(std::move(schema))
{}

Hashset::Hashset(Hashset &&other) : schema(std::move(other.schema)), key_to_rowset(std::move(other.key_to_rowset))
{}

Rowset *Hashset::find(Row *key)
{
    auto it = key_to_rowset.find(*key);
    return it == key_to_rowset.end() ? nullptr : &it->second;
}

Rowset *Hashset::find_or_create(Row *key)
{
    auto it = key_to_rowset.find(*key);
    if (it == key_to_rowset.end()) {
        it = key_to_rowset.insert(std::make_pair(*key, Rowset(schema))).first;
    }
    return &it->second;
}

}    // namespace shdb
